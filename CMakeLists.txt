cmake_minimum_required(VERSION 3.0)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

project(pyfacesdk)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -Wall -fPIC")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static-libgcc -static-libstdc++ -Wl,-rpath,$ORIGIN")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,-Bsymbolic")

SET(CMAKE_AR gcc-ar)
SET(CMAKE_RANLIB gcc-ranlib)

set(Boost_NO_SYSTEM_PATHS TRUE)
if (Boost_NO_SYSTEM_PATHS)
  set(BOOST_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/boost")
  set(BOOST_INCLUDE_DIRS "${BOOST_ROOT}/include")
  set(BOOST_LIBRARY_DIRS "${BOOST_ROOT}/lib")
endif (Boost_NO_SYSTEM_PATHS)

set(Boost_USE_STATIC_LIBS ON)
find_package(PythonLibs 2.7 REQUIRED)
find_package(Boost REQUIRED)
find_package(Boost COMPONENTS python REQUIRED)

set(OpenCV_STATIC ON)
find_package(OpenCV REQUIRED)

find_package(FFmpeg REQUIRED)
find_package(FaceSDK)

set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
find_package(Threads REQUIRED)

find_library(LIBRT_LIBRARIES rt)
if(NOT LIBRT_LIBRARIES)
    message(FATAL_ERROR "librt not found")
endif(NOT LIBRT_LIBRARIES)

#building

add_library(pyfacesdk SHARED src/pyfacesdk.cpp)
target_include_directories(pyfacesdk PUBLIC ${FACESDK_INCLUDE_DIRS} ${PYTHON_INCLUDE_DIRS} ${Boost_INCLUDE_DIR})
target_link_libraries(pyfacesdk ${FACESDK_LIBRARIES}  ${Boost_LIBRARIES})
set_target_properties(pyfacesdk PROPERTIES PREFIX "")

add_library(pyvideoio SHARED
    src/pyvideoio.cpp
    src/videoio/BufferedVideoReader.cpp
    src/videoio/SimpleVideoReader.cpp
    src/videoio/IVideoReader.cpp)
target_include_directories(pyvideoio PUBLIC ${FACESDK_INCLUDE_DIRS} ${PYTHON_INCLUDE_DIRS} ${Boost_INCLUDE_DIR} ${FFMPEG_INCLUDE_DIR})
target_link_libraries(pyvideoio ${Boost_LIBRARIES} ${FFMPEG_LIBRARIES} ${LIBRT_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
set_target_properties(pyvideoio PROPERTIES PREFIX "")

add_library(pytvaio SHARED src/pytvaio.cpp)
target_include_directories(pytvaio PUBLIC ${FACESDK_INCLUDE_DIRS} ${PYTHON_INCLUDE_DIRS} ${Boost_INCLUDE_DIR})
target_link_libraries(pytvaio ${TVAIO_LIBRARIES} ${Boost_LIBRARIES})
set_target_properties(pytvaio PROPERTIES PREFIX "")

add_library(pyfacesdk_tools SHARED src/pyfacesdk_tools.cpp)
target_include_directories(pyfacesdk_tools PUBLIC ${FACESDK_INCLUDE_DIRS} ${PYTHON_INCLUDE_DIRS} ${Boost_INCLUDE_DIR} ${OpenCV_INCLUDE_DIR})
TARGET_LINK_LIBRARIES(pyfacesdk_tools ${Boost_LIBRARIES} ${OpenCV_LIBS})
set_target_properties(pyfacesdk_tools PROPERTIES PREFIX "")
