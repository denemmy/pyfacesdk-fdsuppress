set(FACESDK_INCLUDE_DIRS /opt/facesdk/include)
set(FACESDK_LIBRARIES ${FACESDK_LIBRARIES}
    /opt/facesdk/lib/libfacesdk_static.a
    /opt/facesdk/lib/libcrosstools_library.a
    /opt/facesdk/lib/liblibconvnet_library.a
    /opt/facesdk/lib/liblinear_library.a
    /opt/facesdk/lib/libinih_library.a
    /opt/facesdk/lib/libmkl_core.a
    /opt/facesdk/lib/libmkl_intel_ilp64.a
    /opt/facesdk/lib/libmkl_sequential.a)

set(TVAIO_LIBRARIES ${FACESDK_LIBRARIES} /opt/facesdk/lib/libtvaio_library.so)
