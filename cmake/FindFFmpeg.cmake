set(FFMPEG_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/include/")

set(FFMPEG_LIBRARIES ${FFMPEG_LIBRARIES}
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libavcodec.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libavdevice.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libavfilter.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libavformat.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libavutil.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libpostproc.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libswresample.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/ffmpeg/lib/libswscale.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/v4l2-utils/lib/libv4l2.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/v4l2-utils/lib/libv4l1.a
    ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/v4l2-utils/lib/libv4lconvert.a)
