#include <vector>
#include <algorithm>
#include <iostream>
#include <array>
#include <sstream>
#include <ostream>
#include <memory>
#include <cmath>

/* BOOST */
#define BOOST_PYTHON_STATIC_LIB
#include <boost/python.hpp>

/* FACESDK */
#include "tvacore/image.h"
#include "tvaio/image.h"

using namespace TVA::Core;

using boost::noncopyable;

using boost::python::object;
using boost::python::def;
using boost::python::self;
using boost::python::class_;
using boost::python::arg;
using boost::python::args;
using boost::python::make_constructor;
using boost::python::default_call_policies;
using boost::python::no_init;
using boost::python::scope;
using boost::python::docstring_options;
using boost::python::make_tuple;
using boost::python::extract;

BOOST_PYTHON_MODULE(pytvaio)
{
    scope().attr("__doc__") = "TVAIO module";
    docstring_options docopt;
    docopt.enable_all();
    docopt.disable_cpp_signatures();

    def("imread", &TVA::ImageIO::load, args("filename"), "load image");
    def("imwrite", &TVA::ImageIO::save, args("image", "filename"), "save image");
}
