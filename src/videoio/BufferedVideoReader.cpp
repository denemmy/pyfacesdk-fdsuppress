#include "BufferedVideoReader.h"
#include <iostream>

using namespace TVA;
using namespace VideoIO;
using namespace std;

BufferedVideoReader::BufferedVideoReader(std::string input_name, unsigned int buffer_size) :
    SimpleVideoReader(input_name),
    _buffer_size(int(buffer_size)),
    _write_idx(-1),
    _read_idx(-1),
    _finish_buffer_thread(false),
    _error_buffer_thread(false),
    _last_read_idx(-1),
    _last_write_idx(-1),
    _thread_error_msg("")
{
    if (buffer_size <= 1) {
        throw Core::InvalidArgument("Buffer size should be a positive number greater than 1");
    }
    if (!_is_started) {
        throw VideoError("Error during video initialization");
        // NOTE: this should never be the case if base class constructor worked correctly
        // so it's more for debug so far
    }

    _buffer.resize(buffer_size);
    _buffer_ts.resize(buffer_size);
    _buffer_idx = vector<int64_t>(buffer_size, -1);

    _thread = std::thread(&BufferedVideoReader::bufferThread, this);
}

BufferedVideoReader::~BufferedVideoReader()
{
    _mtx.lock();
    _finish_buffer_thread = true;
    _mtx.unlock();

    // wait until thread is finished
    _thread.join();
}

void BufferedVideoReader::bufferThread()
{
    // loop for acquiring all frames
    while (true) {
        // get command or find out new index for writing
        _mtx.lock();
        if (_finish_buffer_thread) {
            _mtx.unlock();
            break;
        }

        // next index
        _write_idx = (_write_idx + 1) % _buffer_size;

        // skip one index if it is processed by analytics engine
        if (_read_idx == _write_idx)
            _write_idx = (_write_idx + 1) % _buffer_size;

        _mtx.unlock();

        // read frame to specific buffer position
        try {
            if (!SimpleVideoReader::read(_buffer[_write_idx], _buffer_ts[_write_idx]))
                break;
            _buffer_idx[_write_idx] = ++_last_write_idx;
        } catch (const VideoError& e) {
            _error_buffer_thread = true;
            _thread_error_msg = e.what();
            break;
        }
    }
}

BufferedVideoReader& BufferedVideoReader::read(Core::Image8U& out_frame, TimeStamp& out_ts)
{
    auto start_time = std::chrono::system_clock::now();
    while (true) {

        // step into protected area
        _mtx.lock();
        _read_idx = -1;

        // error during buffer thread
        if (_error_buffer_thread) {
            _is_started = false;
            _mtx.unlock();
            throw VideoError(_thread_error_msg);
        }

        // find next frame index: its timestamp should be the next after last one
        int bestInd = -1;
        int64_t bestIdx = 1000000000000;
        for (int i = 0; i < _buffer_size; ++i) {
            if (_buffer_idx[i] > _last_read_idx && _buffer_idx[i] < bestIdx && i != _write_idx) {
                bestInd = i;
                bestIdx = _buffer_idx[i];
            }
        }

        // if best index is good for reading
        if (bestInd >= 0) {
            _read_idx = bestInd;
            _last_read_idx = _buffer_idx[bestInd];
            _mtx.unlock();
            break;
        }

        // wait before next check
        _mtx.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        // if too long inside this reading frame loop then throw error
        auto cur_time = std::chrono::system_clock::now();
        std::chrono::duration<double> diff_time = cur_time - start_time;
        if (diff_time.count() > _max_read_time) {
            _mtx.lock();
            _finish_buffer_thread = true;
            _is_started = false;
            _mtx.unlock();
            throw VideoError("Too much time spent during reading");
        }
    }

    // copy frame from buffer in a light way
    out_frame = _buffer[_read_idx].lightCopy();
    out_ts = _buffer_ts[_read_idx];

    return *this;
}