#pragma once
#include "tvacore/exceptions.hpp"
#include <cstdint>
#include "tvacore/exceptions.hpp"

namespace TVA {
namespace VideoIO {

    /*!
        \defgroup VideoInputOutput Video Input and Output
        Video engine.
    */

    //!  Exception during work with video
    /*!
      \ingroup VideoInputOutput
      Exception throwed by video readers and writers.
      After this exception video can no longer be read or write with current object.
    */
    class VideoError: public TVA::Core::TVAError
    {
    public:
        VideoError(const std::string &what): TVA::Core::TVAError(what) {}
    };

    //!  Type for video frame time stamp
    /*!
      \ingroup VideoInputOutput
      Frame time stamp is an integer representing milliseconds.
    */
    typedef std::int64_t TimeStamp;

} }