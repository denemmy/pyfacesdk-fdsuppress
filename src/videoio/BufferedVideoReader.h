#pragma once

#include "SimpleVideoReader.h"
#include <vector>
#include <thread>
#include <mutex>

namespace TVA {
namespace VideoIO {

class BufferedVideoReader : public SimpleVideoReader
{
public:

    BufferedVideoReader(std::string input_name, unsigned int buffer_size);
    virtual ~BufferedVideoReader();

    virtual BufferedVideoReader& read(Core::Image8U& out_frame, TimeStamp& out_ts);

private:
    int _buffer_size;
    std::vector<Core::Image8U> _buffer;
    std::vector<TimeStamp> _buffer_ts;
    std::vector<std::int64_t> _buffer_idx;

    std::mutex _mtx;
    int _write_idx, _read_idx;
    bool _finish_buffer_thread, _error_buffer_thread;
    std::int64_t _last_read_idx, _last_write_idx;
    std::string _thread_error_msg;

    std::thread _thread;
    void bufferThread();
};
}}