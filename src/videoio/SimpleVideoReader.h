#pragma once

#include "IVideoReader.h"

extern "C" {
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/channel_layout.h>
#include <libavutil/error.h>
#include <libavutil/log.h>
#include <libavutil/mathematics.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libavdevice/avdevice.h>
}
#include <thread>


namespace TVA {
namespace VideoIO {

class SimpleVideoReader : public IVideoReader
{
public:
    SimpleVideoReader(std::string input_name);
    virtual ~SimpleVideoReader();

    virtual SimpleVideoReader& read(Core::Image8U& out_frame, TimeStamp& out_ts);
    virtual operator bool() const;

    virtual int getWidth() const;
    virtual int getHeight() const;
    virtual double getFPS() const;
protected:

    struct SInterruptData
    {
        std::chrono::time_point<std::chrono::system_clock> start_time;
        bool inside_reading_function;
        bool too_much_inside;
        int threshold;

        SInterruptData() :
            start_time(),
            inside_reading_function(false),
            too_much_inside(false),
            threshold(0)
        {}
    };
    static int interrupt_cb(void *ctx);

    TimeStamp _time_stamp;
    bool _is_started, _can_read_packets;

    AVFormatContext *_pIFC;
    SInterruptData _interrupt_data;
    int _video_stream_idx;
    AVCodecContext *_pVideoCC;
    AVCodec *_pVideoCodec;
    AVFrame *_pFrame, *_pFrameRGB;
    uint8_t *_pBuffer;
    struct SwsContext *_pVideoSWS;

    int _video_width, _video_height;
    bool _is_just_started;
    double _fps;

    // max duration of read function in seconds
    const double _max_read_time = 10.0;
};


} }
