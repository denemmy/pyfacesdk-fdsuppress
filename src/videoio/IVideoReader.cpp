#include "IVideoReader.h"
#include "SimpleVideoReader.h"
#include "BufferedVideoReader.h"

using namespace TVA::VideoIO;
using namespace std;

shared_ptr<IVideoReader> IVideoReader::create(string input_name, unsigned int buffer_size)
{
    if (buffer_size <= 1)
        return shared_ptr<SimpleVideoReader>(new SimpleVideoReader(input_name));
    else
        return shared_ptr<BufferedVideoReader>(new BufferedVideoReader(input_name, buffer_size));
}

std::string getName(int device_num)
{
    char buffer[256] = {0};
    sprintf(buffer, "/dev/video%d", device_num);
    return std::string(buffer);
}

shared_ptr<IVideoReader> IVideoReader::create(int device_num, unsigned int buffer_size)
{
    char input_name[256] = {0};
    sprintf(input_name, "/dev/video%d", device_num);
    return create(input_name, buffer_size);
}
