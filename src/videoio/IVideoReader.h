/*
* Video Reader Interface
*
* This file is a part of TVAIO library
*
* Video Analysis Technologies (c)
*/

#pragma once
#ifndef TVAIO_EXPORT
#define TVAIO_EXPORT
#endif

#include "VideoCommon.h"
#include <memory>
#include <string>
#include "tvacore/image.h"

namespace TVA {
namespace VideoIO {

    //!  Video Reader Interface
    /*!
      \ingroup VideoInputOutput
      Public interface for video reading.
      The class is not thread-safe, an external synchronisation is required.
    */
    class IVideoReader {
    public:
        static TVAIO_EXPORT std::shared_ptr<IVideoReader> create(std::string input_name, unsigned int buffer_size = 0);
        static TVAIO_EXPORT std::shared_ptr<IVideoReader> create(int device_num, unsigned int buffer_size = 0);

        virtual ~IVideoReader() = default;

        virtual TVAIO_EXPORT IVideoReader& read(Core::Image8U& out_frame, TimeStamp& out_ts) = 0;
        virtual TVAIO_EXPORT operator bool() const  = 0;

        virtual TVAIO_EXPORT int getWidth() const = 0;
        virtual TVAIO_EXPORT int getHeight() const = 0;
        virtual TVAIO_EXPORT double getFPS() const = 0;
    };
} }