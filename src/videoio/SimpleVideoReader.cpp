#include "SimpleVideoReader.h"
#include <iostream>

using namespace TVA;
using namespace VideoIO;
using namespace std;

SimpleVideoReader::SimpleVideoReader(std::string input_name) :
    _time_stamp(0),
    _is_started(false),
    _can_read_packets(false),
    _pIFC(nullptr),
    _interrupt_data(),
    _video_stream_idx(0),
    _pVideoCC(nullptr),
    _pVideoCodec(nullptr),
    _pFrame(nullptr),
    _pFrameRGB(nullptr),
    _pBuffer(nullptr),
    _pVideoSWS(nullptr),
    _video_width(0),
    _video_height(0),
    _is_just_started(false),
    _fps(0)
{
    //! TODO: this should be called only once for an entire program
    av_register_all();
    avdevice_register_all();
    avformat_network_init();
    avcodec_register_all();

    // disable logging to stderr
    av_log_set_level(AV_LOG_QUIET);
    //av_log_set_level(AV_LOG_VERBOSE);

    // input format context
    _pIFC = avformat_alloc_context();

    // set interrupt callback
    _interrupt_data.inside_reading_function = false;
    _interrupt_data.too_much_inside = false;
    _interrupt_data.threshold = int(_max_read_time);
    const AVIOInterruptCB int_cb = { interrupt_cb, &_interrupt_data };
    _pIFC->interrupt_callback = int_cb;

    AVDictionary *p_dict = nullptr;
    AVInputFormat *p_av_input_format = nullptr;

    // use tcp as rtsp transport for IP cams
    if(input_name.substr(0, 7) == "rtsp://") {
        av_dict_set(&p_dict, "rtsp_transport", "tcp", 0);
    }

    // for v4l2 devices
    if(input_name.substr(0, 4) == "/dev") {
        p_av_input_format = av_find_input_format("video4linux2");
        av_dict_set(&p_dict, "video_size", "1920x1080", 0);
    }

    // open input
    _interrupt_data.start_time = std::chrono::system_clock::now();
    _interrupt_data.inside_reading_function = true;
    if (avformat_open_input(&_pIFC, input_name.c_str(), p_av_input_format, &p_dict) < 0) {
        _interrupt_data.inside_reading_function = false;
        av_dict_free(&p_dict);
        throw VideoError("Could not open input");
    }
    _interrupt_data.inside_reading_function = false;
    av_dict_free(&p_dict);

    // find input stream info
    if (avformat_find_stream_info(_pIFC, 0) < 0) {
        throw VideoError("Failed to retrieve input stream information");
    }

    // find video stream index, if many choose the first
    _video_stream_idx = -1;
    for (unsigned int i = 0; i < _pIFC->nb_streams; i++) {
        if (_pIFC->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            _video_stream_idx = i;
            break;
        }
    }
    if (_video_stream_idx < 0) {
        throw VideoError("Cannot find video stream");
    }

    // initialize decoder for video
    _pVideoCC = _pIFC->streams[_video_stream_idx]->codec;
    _pVideoCodec = avcodec_find_decoder(_pVideoCC->codec_id);
    if (_pVideoCodec == NULL) {
        throw VideoError("Cannot find video codec");
    }
    if (avcodec_open2(_pVideoCC, _pVideoCodec, NULL) < 0) {
        throw VideoError("Cannot open video codec");
    }

    // allocate frames
    _pFrame = av_frame_alloc();
    _pFrameRGB = av_frame_alloc();
    
    int numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGB24, _pVideoCC->width, _pVideoCC->height, 1);
    _pBuffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));
    av_image_fill_arrays(_pFrameRGB->data, _pFrameRGB->linesize, _pBuffer, AV_PIX_FMT_RGB24, _pVideoCC->width, _pVideoCC->height, 1);

    // set converter to rgb
    _pVideoSWS = sws_getCachedContext(NULL, _pVideoCC->width, _pVideoCC->height, _pVideoCC->pix_fmt,
        _pVideoCC->width, _pVideoCC->height, AV_PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);

    _video_width = _pVideoCC->height;
    _video_height = _pVideoCC->width;
    if (_pIFC->streams[_video_stream_idx]->avg_frame_rate.den == 0)
        _fps = av_q2d(_pIFC->streams[_video_stream_idx]->r_frame_rate);
    else
        _fps = av_q2d(_pIFC->streams[_video_stream_idx]->avg_frame_rate);

    _time_stamp = 0;
    _is_just_started = true;
    _is_started = true;
    _can_read_packets = true;
}

SimpleVideoReader::~SimpleVideoReader()
{
    if (_pIFC != nullptr) {
        avformat_close_input(&_pIFC);
        _pIFC = nullptr;
    }
    if (_pFrame != nullptr) {
        av_free(_pFrame);
        _pFrame = nullptr;
    }
    if (_pFrameRGB != nullptr) {
        if (_pFrameRGB->data[0] != nullptr)
            av_freep(&_pFrameRGB->data[0]);
        av_free(_pFrameRGB);
        _pFrameRGB = nullptr;
    }
    if (_pVideoCC != nullptr) {
        //avcodec_close(m_pVideoCC);
        //av_free(m_pVideoCC);
        _pVideoCC = nullptr;
    }
    if (_pVideoSWS != nullptr) {
        sws_freeContext(_pVideoSWS);
        _pVideoSWS = nullptr;
    }
}

// this functions is passed to ffmpeg as a callback
// it is a special function which is called by ffmpeg frequently during video frame acquiring
// if it does not return 0, then ffmpeg stops frame acquiring with an error
// it allows to control situations when frame acquiring is too long
int SimpleVideoReader::interrupt_cb(void *ctx)
{
    SInterruptData* pData = (SInterruptData*)ctx;
    std::chrono::duration<double> tDiff = std::chrono::system_clock::now() - pData->start_time;
    if (pData->inside_reading_function && tDiff.count() > pData->threshold) {
        pData->too_much_inside = true;
        return 1;
    } else {
        return 0;
    }
}

SimpleVideoReader& SimpleVideoReader::read(Core::Image8U& out_frame, std::int64_t& ts)
{
    if (!_is_started) {
        throw VideoError("SimpleVideoReader has not been started");
    }

    // packet for to read in
    AVPacket pkt;
    av_init_packet(&pkt);

    // loop until frame is obtained or too much time inside loop
    int frameFinished = 0;
    auto start_time = std::chrono::system_clock::now();
    do {

        // if too long inside this reading frame loop then throw error
        std::chrono::duration<double> diff_time = std::chrono::system_clock::now() - start_time;
        if (diff_time.count() > _max_read_time) {
            throw VideoError("Too much time spent during reading");
        }

        if (_can_read_packets) {
            // read packet
            _interrupt_data.start_time = std::chrono::system_clock::now();
            _interrupt_data.inside_reading_function = true;
            int ret = av_read_frame(_pIFC, &pkt);
            _interrupt_data.inside_reading_function = false;

            // check if reading of input went fine
            if (ret < 0) {
                _can_read_packets = false;
                continue;
            }

            // check if reading was too long
            if (_interrupt_data.too_much_inside) {
                throw VideoError("Too much time spent during reading");
            }

            // NOTE: so far only one video stream can be processed
            if (pkt.stream_index != _video_stream_idx) {
                av_packet_unref(&pkt);
                continue;
            }

            // wait for keyframe to start processing
            bool is_key_frame = pkt.flags & AV_PKT_FLAG_KEY;
            if (_is_just_started) {
                if (is_key_frame)
                    _is_just_started = false;
                else {
                    av_packet_unref(&pkt);
                    continue;
                }
            }

            // decode frame
            if (avcodec_decode_video2(_pVideoCC, _pFrame, &frameFinished, &pkt) < 0) {
                throw VideoError("Error during decoding");
            }

        } else {    // _can_read_packets == false

            // decode residual frames, that left in FFMpeg stack, using empty packet
            pkt.data = nullptr;
            pkt.size = 0;
            _interrupt_data.start_time = std::chrono::system_clock::now();
            _interrupt_data.inside_reading_function = true;
            int ret = avcodec_decode_video2(_pVideoCC, _pFrame, &frameFinished, &pkt);
            _interrupt_data.inside_reading_function = false;
            if (ret < 0 || (ret == 0 && frameFinished == 0) || _interrupt_data.too_much_inside) {
                _is_started = false;
                return *this;
            }
        }
    } while (frameFinished == 0);

    // allocate output frame
    out_frame = Core::Image8U(_pFrame->width, _pFrame->height, 3);

    // convert to RGB
    unsigned char* ptemp = out_frame.data(0);
    sws_scale(_pVideoSWS, _pFrame->data, _pFrame->linesize, 0, _pVideoCC->height, (uint8_t* const*)(&ptemp), _pFrameRGB->linesize);

    // compute frame timestamp in milliseconds
    std::int64_t bets = av_frame_get_best_effort_timestamp(_pFrame);
    if (bets >= 0)
        ts = std::int64_t(bets * av_q2d(_pIFC->streams[_video_stream_idx]->time_base) * 1000);
    else
        ts = 0;

    av_packet_unref(&pkt);
    return *this;
}

double SimpleVideoReader::getFPS() const
{
    if (!_is_started) {
        throw VideoError("VideoReader has not been started");
    }

    return _fps;
}

int SimpleVideoReader::getWidth() const
{
    if (!_is_started) {
        throw VideoError("VideoReader has not been started");
    }

    return _video_width;
}

int SimpleVideoReader::getHeight() const
{
    if (!_is_started) {
        throw VideoError("VideoReader has not been started");
    }

    return _video_height;
}

SimpleVideoReader::operator bool() const
{
    return _is_started;
}
