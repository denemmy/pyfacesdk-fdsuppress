#include <vector>
#include <algorithm>
#include <iostream>
#include <array>
#include <sstream>
#include <ostream>
#include <memory>
#include <cmath>

/* OpenCV */
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

/* BOOST */
#define BOOST_PYTHON_STATIC_LIB
#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/intrusive/options.hpp>

#include "array_indexing_suite.hpp"

/* NUMPY */
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarrayobject.h>

/* FACESDK */
#include "tvacore/image.h"
#include "tvacore/point.h"
#include "tvacore/rectangle.h"
#include "tvacore/scored_rectangle.h"
#include "tvaio/image.h"

using namespace TVA::Core;

using boost::noncopyable;

using boost::python::object;
using boost::python::def;
using boost::python::class_;
using boost::python::enum_;
using boost::python::handle;
using boost::python::self;
using boost::python::make_constructor;
using boost::python::init;
using boost::python::no_init;
using boost::python::bases;
using boost::python::default_call_policies;
using boost::python::arg;
using boost::python::args;
using boost::python::vector_indexing_suite;
using boost::python::scope;
using boost::python::docstring_options;

using std::shared_ptr;
using std::make_shared;
using std::vector;
using std::istringstream;
using std::ostringstream;
using std::min;
using std::max;
using std::abs;
using std::uint8_t;
using std::uint32_t;
using std::size_t;
using std::tuple;

object skinmap(const Image8U &image)
{
    size_t width = image.width();
    size_t height = image.height();

    npy_intp dims[2] = {
        npy_intp(height),
        npy_intp(width)
    };

    PyObject *py_array = PyArray_SimpleNew(2, dims, NPY_BOOL);
    uint8_t *data = reinterpret_cast<uint8_t *>(PyArray_DATA(reinterpret_cast<PyArrayObject *>(py_array)));
    const auto rowsize = PyArray_STRIDE(reinterpret_cast<PyArrayObject *>(py_array), 0);

    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            uint8_t r = image(x, y, 0);
            uint8_t g = image(x, y, 1);
            uint8_t b = image(x, y, 2);

            float cb = -0.148 * r - 0.291 * g + 0.439 * b + 128;
            float cr = 0.439 * r - 0.368 * g - 0.071 * b + 128;

            bool ycbcr_skin = 80.0 <= cb && cb <= 120.0 && 133.0 <= cr && cr <= 173.0;

            data[y * rowsize + x] = ycbcr_skin;
        }
    }

    return object(handle<>(py_array));
}

class CV_Detector
{
    cv::CascadeClassifier _cascade;
public:
    CV_Detector(const std::string &fileName):
        _cascade(fileName)
    {
    }

    virtual ~CV_Detector() {
    }

    vector<IScoredRect> detect(const Image8U &image, float scaleFactor = 1.05, const int minNeighbors = 6)
    {
        vector<IScoredRect> result;

        cv::Mat img(image.height(), image.width(), CV_MAKETYPE(CV_8U,3), const_cast<unsigned char *>(image.data()), image.step());
        cv::Mat img_gray(img.size(), CV_8UC1);
        cv::cvtColor(img, img_gray, CV_RGB2GRAY);

        vector<cv::Rect> cv_detections;

        _cascade.detectMultiScale(img_gray, cv_detections, scaleFactor, minNeighbors, 0|CV_HAAR_SCALE_IMAGE, cv::Size(24, 24));

        for(const auto &rect : cv_detections) {
            result.push_back(IScoredRect(rect.x, rect.y, rect.width, rect.height));
        }

        return result;
    }
};

BOOST_PYTHON_MODULE(pyfacesdk_tools)
{
    scope().attr("__doc__") = "FaceSDK Toolbox";
    docstring_options docopt;
    docopt.enable_all();
    docopt.disable_cpp_signatures();

    import_array();

    def("skinmap", skinmap, args("image"), "detect skin regions in image");

    class_<CV_Detector, noncopyable>("CV_Detector", init<std::string>())
        .def("detect", &CV_Detector::detect, args("image", "scaleFactor", "minNeighbors"), "detect objects in image")
    ;
}
