#include <vector>
#include <algorithm>
#include <iostream>
#include <array>
#include <sstream>
#include <ostream>
#include <memory>
#include <cmath>

/* BOOST */
#define BOOST_PYTHON_STATIC_LIB
#include <boost/python.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/intrusive/options.hpp>

#include "array_indexing_suite.hpp"

/* NUMPY */
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarrayobject.h>

/* FACESDK */
#include "tvacore/image.h"
#include "tvacore/point.h"
#include "tvacore/rectangle.h"

#include "facesdk/md_detection/IDetector.h"
#include "facesdk/md_features/IPointsDetector.h"
#include "facesdk/md_recognition/IExtractor.h"
#include "facesdk/md_recognition/IMatcher.h"
#include "facesdk/md_recognition/IPopulation.h"
#include "facesdk/md_recognition/IClassifier.h"
#include "facesdk/md_quality/IQualityEvaluator.h"
#include "facesdk/md_tracking/Tracklet.h"
#include "facesdk/md_tracking/ITracker.h"
#include "facesdk/md_tracking/ITrackMatcher.h"
#include "facesdk/md_tracking/IGlobalTracker.h"
#include "facesdk/md_filter/IDetectionFilter.h"
#include "facesdk/md_core/config_manager.h"

using namespace FaceSDK;
using namespace TVA::Core;

using boost::noncopyable;

using boost::python::object;
using boost::python::def;
using boost::python::class_;
using boost::python::enum_;
using boost::python::handle;
using boost::python::self;
using boost::python::make_constructor;
using boost::python::init;
using boost::python::no_init;
using boost::python::bases;
using boost::python::default_call_policies;
using boost::python::arg;
using boost::python::args;
using boost::python::vector_indexing_suite;
using boost::python::scope;
using boost::python::docstring_options;
using boost::python::register_ptr_to_python;

using std::shared_ptr;
using std::make_shared;
using std::vector;
using std::istringstream;
using std::ostringstream;
using std::min;
using std::max;
using std::uint8_t;
using std::size_t;

using std::memcpy;

ConfigManager mngr;

shared_ptr<Image8U> Image8U_Create(const object &image)
{
    PyArrayObject *img=(PyArrayObject*) image.ptr();
    PyArray_Descr *descr = PyArray_DESCR(img);

    if(descr->kind != 'u')
        throw TVA::Core::TVAError("wrong ndarray type, 8bit unsigned requested");

    uint8_t *data = nullptr;
    int width = 0, height = 0, channels = 1, step = 0;

    int dims = PyArray_NDIM(img);

    if(dims == 2) {
        channels = 1;
    } else if(dims == 3) {
        channels = 3;
    } else {
        throw TVA::Core::TVAError("wrong channel number in image");
    }

    width = PyArray_DIMS(img)[1];
    height = PyArray_DIMS(img)[0];

    data = static_cast<uint8_t *>(PyArray_DATA(img));
    step = PyArray_STRIDE(img, 0);

    return make_shared<Image8U>(data, width, height, channels, step);
}

object Image8U_blob(const Image8U &image)
{
    npy_intp dims[3] = {
        npy_intp(image.height()),
        npy_intp(image.width()),
        npy_intp(image.channels())
    };

    PyObject *py_array = PyArray_SimpleNew(image.channels(), dims, NPY_UINT8);
    uint8_t *data = reinterpret_cast<uint8_t *>(PyArray_DATA(reinterpret_cast<PyArrayObject *>(py_array)));
    const auto rowsize = PyArray_STRIDE(reinterpret_cast<PyArrayObject *>(py_array), 0);

    for(decltype(image.height()) y = 0; y < image.height(); ++y) {
        memcpy(data + y * rowsize, image.data(y), sizeof(data[0]) * image.step());
    }

    return object(handle<>(py_array));
}

shared_ptr<IDetector> IDetector_Create(const DETECTOR_TYPE type)
{
    return IDetector::create(mngr, type);
}

vector<IScoredRect> IDetector_detect(IDetector &self, const Image8U &image, float precision_level)
{
    int max_face = min(image.width(), image.height());
    int min_face = min(32, max_face);
    return self.detect(image, URect(0, 0, image.width(), image.height()), USize(min_face, min_face), USize(max_face, max_face), precision_level);
}

shared_ptr<IQualityEvaluator> IQualityEvaluator_Create(const QUALITY_ESTIMATOR_TYPE type)
{
    return IQualityEvaluator::create(mngr, type);
}

shared_ptr<ITracker> ITracker_Create(const TRACKING_TYPE type)
{
    return ITracker::create(mngr, type);
}

shared_ptr<ITrackMatcher> ITrackMatcher_Create()
{
    return ITrackMatcher::create(mngr);
}

shared_ptr<IGlobalTracker> IGlobalTracker_Create(const TRACKING_TYPE type)
{
    return IGlobalTracker::create(mngr, type);
}

shared_ptr<IDetectionFilter> IDetectionFilter_Create(const DETECTION_FILTER_TYPE type)
{
    return IDetectionFilter::create(mngr, type);
}

Image8U DetectionCroppedFrame_getter(const DetectionCroppedFrame &image)
{
    return *image.image;
}

shared_ptr<IPointsDetector> IPointsDetector_Create(const POINTS_DETECTOR_TYPE type)
{
    return IPointsDetector::create(mngr, type);
}

shared_ptr<Descriptor> Descriptor_Create(const object &obj)
{
    PyObject *pyBuffer = obj.ptr();
    if (!PyBuffer_Check(pyBuffer))
        throw TVAError("invalid type");

    Py_buffer view;
    if(PyObject_GetBuffer(pyBuffer, &view, PyBUF_SIMPLE) != 0)
        throw TVAError("broken buffer");

    auto desc = make_shared<Descriptor>(0);
    istringstream reader;
    reader.rdbuf()->pubsetbuf(reinterpret_cast<char *>(view.buf), view.len);
    reader >> *desc;
    PyBuffer_Release(&view);
    return desc;
}

object Descriptor_buffer(const Descriptor &desc)
{
    PyObject *buf = PyBuffer_New(desc.size() + sizeof(desc.size()));

    Py_buffer view;
    if(PyObject_GetBuffer(buf, &view, PyBUF_SIMPLE) != 0)
        throw TVAError("broken buffer");

    ostringstream writer;
    writer.rdbuf()->pubsetbuf(reinterpret_cast<char *>(view.buf), view.len);
    writer << desc;
    return object(handle<>(buf));
}

shared_ptr<IExtractor> IExtractor_Create(const RECOGNITION_ALGO type)
{
    return IExtractor::create(mngr, type);
}

shared_ptr<IMatcher> IMatcher_Create(const RECOGNITION_ALGO type)
{
    return IMatcher::create(mngr, type);
}

shared_ptr<IClassifier> IClassifier_Create(const RECOGNITION_ALGO type)
{
    return IClassifier::create(mngr, type);
}

#define EXPORT_VECTOR(Type) class_<vector<Type>>(#Type "_List") .def(vector_indexing_suite<vector<Type>>())
#define EXPORT_ARRAY(Type) class_<Type>(#Type) .def(array_indexing_suite<Type>())

#define GENERATE_WRAPPERS(WRAPPER) WRAPPER(I); WRAPPER(U); WRAPPER(F); WRAPPER(D)

typedef std::array<float, 1> Array1;
typedef std::array<float, 2> Array2;
typedef std::array<float, 3> Array3;
typedef std::array<float, 4> Array4;
typedef std::array<float, 5> Array5;
typedef std::array<float, 6> Array6;
typedef std::array<float, 7> Array7;
typedef std::array<float, 8> Array8;
typedef std::array<float, 9> Array9;

BOOST_PYTHON_MODULE(pyfacesdk)
{
    EXPORT_ARRAY(Array1);
    EXPORT_ARRAY(Array2);
    EXPORT_ARRAY(Array3);
    EXPORT_ARRAY(Array4);
    EXPORT_ARRAY(Array5);
    EXPORT_ARRAY(Array6);
    EXPORT_ARRAY(Array7);
    EXPORT_ARRAY(Array8);
    EXPORT_ARRAY(Array9);

    scope().attr("__doc__") = "pyFaceSDK: Biometric SDK";
    docstring_options docopt;
    docopt.enable_all();
    docopt.disable_cpp_signatures();

    import_array();

    class_<Image8U>("Image8U", no_init)
        .def("__init__", make_constructor(&Image8U_Create))
        .def_readonly("width", &Image8U::width, "image width")
        .def_readonly("height", &Image8U::height, "image height")
        .def_readonly("channels", &Image8U::channels, "image channels")
        .def_readonly("center", &Image8U::center, "image center")
        .add_property("blob", &Image8U_blob, "numpy.ndarray data copy")
        .def("__call__", static_cast<Image8U::channel_t (Image8U::*)(size_t, size_t, size_t) const>(&Image8U::operator()), args("x", "y", "channel"), "pixel access")
        .def("copy", &Image8U::copy, "deep copy")
        .def("crop", static_cast<Image8U (Image8U::*)(size_t, size_t, size_t, size_t) const>(&Image8U::crop), args("x", "y", "w", "h"), "deep crop")
        .def("lightCrop", static_cast<Image8U (Image8U::*)(size_t, size_t, size_t, size_t)>(&Image8U::lightCrop), args("x", "y", "w", "h"), "light crop without data copy")
        .def("clamp", &Image8U::clamp, args("min_value", "max_value"), "clamp pixels in range")
        .def("reflectHorizontally", &Image8U::reflectHorizontally, "reflectHorizontally")
        .def("reflectVertically", &Image8U::reflectVertically, "reflectVertically")
        .def("split", &Image8U::split, "split image to channels")
        .def("channel", &Image8U::channel, args("layer_idx"), "extract specified channel")
        .def(self == self)
        .def(self != self)
        .def(self + self)
        .def(self - self)
        .def(self * long())
        .def(self / long())
        .def(self * float())
        .def(self / float())
        .def(!self)
    ;
    EXPORT_VECTOR(Image8U);

    /* Rectangle class wrapper */

    #define RECTANGLE_WRAPPER(Type) \
    class_<Type##Rect>(#Type "Rect", \
            init< \
                decltype(Type##Rect::x), \
                decltype(Type##Rect::y), \
                decltype(Type##Rect::w), \
                decltype(Type##Rect::h) \
            >()) \
        .def_readwrite("x", &Type##Rect::x, "x") \
        .def_readwrite("y", &Type##Rect::y, "y") \
        .def_readwrite("w", &Type##Rect::w, "width") \
        .def_readwrite("h", &Type##Rect::h, "height") \
        .def("area", &Type##Rect::area, "area") \
        .def("IoU", static_cast<double(Type##Rect::*)(const Type##Rect &) const>(&Type##Rect::IoU), args("r"), "Intersection over union") \
        .def(self & self) \
        .def(self &= self) \
        .def(self | self) \
        .def(self |= self) \
        .def(self == self) \
        .def(self != self) \
    ; \
    def("IoU", static_cast<double(*)(const Type##Rect &, const Type##Rect &)>(Type##Rect::IoU), args("l", "r"), "Intersection over union"); \
    EXPORT_VECTOR(Type##Rect)

    GENERATE_WRAPPERS(RECTANGLE_WRAPPER);

    #define SCORED_RECTANGLE_WRAPPER(Type) \
    class_<Type##ScoredRect, bases<Type##Rect>>(#Type "ScoredRect", \
            init< \
                decltype(Type##ScoredRect::x), \
                decltype(Type##ScoredRect::y), \
                decltype(Type##ScoredRect::w), \
                decltype(Type##ScoredRect::h), \
                decltype(Type##ScoredRect::score) \
            >()) \
        .def_readwrite("score", &Type##ScoredRect::score, "score") \
    ; \
    EXPORT_VECTOR(Type##ScoredRect)

    GENERATE_WRAPPERS(SCORED_RECTANGLE_WRAPPER);

    #define POINT_WRAPPER(Type) \
    class_<Type##Point>(#Type "Point", init<decltype(Type##Point::x), decltype(Type##Point::y)>()) \
        .def_readwrite("x", &Type##Point::x, "x") \
        .def_readwrite("y", &Type##Point::y, "y") \
        .def("distance", static_cast<double(Type##Point::*)(const Type##Point &) const>(&Type##Point::distance), args("p"), "distance to other point") \
        .def(self + self) \
        .def(self += self) \
        .def(self - self) \
        .def(self -= self) \
        .def(self == self) \
        .def(self != self) \
        .def(decltype(Type##Point::x)() * self) \
    ; \
    def("distance", static_cast<double(*)(const Type##Point &, const Type##Point &)>(Type##Point::distance), args("p1", "p2"), "distance between points"); \
    EXPORT_VECTOR(Type##Point)

    GENERATE_WRAPPERS(POINT_WRAPPER);

    /* FaceDetector */
    enum_<DETECTOR_TYPE>("DETECTOR_TYPE")
        .value("DEFAULT", DETECTOR_TYPE::DEFAULT)
    ;
    class_<IDetector, noncopyable>("IDetector", no_init)
        .def("__init__", make_constructor(&IDetector_Create, default_call_policies(), (arg("type") = DETECTOR_TYPE::DEFAULT)))
        .def("clearPrealloc", &IDetector::clearPrealloc, "clear preallocated data")
        .def("detect", &IDetector_detect, args("image", "precision_level"), "detect faces on image")
        .def("detect", &IDetector::detect, args("image", "roi", "min_size", "max_size", "precision_level"), "detect faces on image")
    ;

    /* Quality */
    class_<FaceQualityConfigure>("FaceQualityConfigure")
        .def_readwrite("overall", &FaceQualityConfigure::overall, "estimate overall score")
        .def_readwrite("resolution", &FaceQualityConfigure::resolution, "estimate face resolution score")
        .def_readwrite("context", &FaceQualityConfigure::context, "estimate face context score")
        .def_readwrite("angles", &FaceQualityConfigure::angles, "estimate face rotation score")
    ;

    class_<FaceAngles>("FaceAngles", no_init)
        .def_readonly("yaw", &FaceAngles::yaw, "yaw")
        .def_readonly("pitch", &FaceAngles::pitch, "pitch")
        .def_readonly("roll", &FaceAngles::roll, "roll")
    ;

    class_<FaceQuality>("FaceQuality", no_init)
        .def_readonly("overall", &FaceQuality::overall, "overall score")
        .def_readonly("resolution", &FaceQuality::resolution, "resolution score")
        .def_readonly("context", &FaceQuality::context, "context score")
        .def_readonly("angles", &FaceQuality::angles, "angles score")
    ;

    enum_<QUALITY_ESTIMATOR_TYPE>("QUALITY_ESTIMATOR_TYPE")
        .value("DEFAULT", QUALITY_ESTIMATOR_TYPE::DEFAULT)
    ;

    class_<IQualityEvaluator, noncopyable>("IQualityEvaluator", no_init)
        .def("__init__", make_constructor(&IQualityEvaluator_Create, default_call_policies(), (arg("type") = QUALITY_ESTIMATOR_TYPE::DEFAULT)))
        .add_property("config", &IQualityEvaluator::getConfig, &IQualityEvaluator::configure, "enabled face quality modules")
        .def("evaluate", &IQualityEvaluator::evaluate, args("image", "bbox"), "evaluate image quality")
    ;

    /* Tracking */

    class_<Descriptor>("Descriptor", no_init)
        .def("__init__", make_constructor(&Descriptor_Create))
        .add_property("size", &Descriptor::size, "descriptor size in bytes")
        .def("buffer", &Descriptor_buffer, "bytebuffer")
    ;
    register_ptr_to_python<std::shared_ptr<Descriptor>>();

    enum_<TRACKING_TYPE>("TRACKING_TYPE")
        .value("DEFAULT", TRACKING_TYPE::DEFAULT)
    ;
    class_<ITracker, noncopyable>("ITracker", no_init)
        .def("__init__", make_constructor(&ITracker_Create, default_call_policies(), (arg("type") = TRACKING_TYPE::DEFAULT)))
        .def("init", &ITracker::init, args("image", "bbox"), "This method should be called before calling track() method")
        .def("track", &ITracker::track, args("image"), "Track face in a new frame")
        .def("setRelativeWindowSize", static_cast<void (ITracker::*)(float)>(&ITracker::setRelativeWindowSize), args("size"), "sets search window size relative to face size")
        .def("setAbsoluteWindowSize", static_cast<void (ITracker::*)(unsigned int)>(&ITracker::setAbsoluteWindowSize), args("size"), "sets search window size in pixels")
    ;

    //enum_<TRACK_MATCHER_TYPE>("TRACK_MATCHER_TYPE")
    //    .value("DEFAULT", TRACK_MATCHER_TYPE::DEFAULT)
    //;
    class_<ITrackMatcher, noncopyable>("ITrackMatcher", no_init)
        .def("__init__", make_constructor(&ITrackMatcher_Create))
        .def("match", &ITrackMatcher::match, args("set1", "set2", "minimal_iou"), "Match two sets of face tracks")
    ;

    enum_<TRACKLET_STATE>("TRACKLET_STATE")
        .value("ALIVE", TRACKLET_STATE::ALIVE)
        .value("LOST", TRACKLET_STATE::LOST)
        .value("DEAD", TRACKLET_STATE::DEAD)
    ;

    class_<DetectionCroppedFrame>("DetectionCroppedFrame", no_init)
        .add_property("image", &DetectionCroppedFrame_getter, "image contents")
        .def_readonly("quality", &DetectionCroppedFrame::quality, "image quality score")
        .def_readonly("bbox", &DetectionCroppedFrame::bbox, "face bbox")
        .def_readonly("frame_ts", &DetectionCroppedFrame::frame_ts, "timestamp")
    ;
    EXPORT_VECTOR(DetectionCroppedFrame);

    class_<Tracklet, noncopyable>("Tracklet", no_init)
        .def_readonly("start_time", &Tracklet::start_time, "track start time")
        .def_readonly("finish_time", &Tracklet::finish_time, "track finish time")
        .def_readonly("state", &Tracklet::state, "track state")
        .def_readonly("best_frames", &Tracklet::best_frames, "best frames for track")
        .def_readonly("position", &Tracklet::position, "current position")
        .def_readonly("id", &Tracklet::id, "track id")
    ;
    EXPORT_VECTOR(Tracklet);

    class_<IGlobalTracker, noncopyable>("IGlobalTracker", no_init)
        .def("__init__", make_constructor(&IGlobalTracker_Create, default_call_policies(), (arg("type") = TRACKING_TYPE::DEFAULT)))
        .def("process", &IGlobalTracker::process, args("frame", "timestamp"), "Process new frame with tracker")

        .add_property("tracklets", &IGlobalTracker::getTracklets, "get current tracklets")

        .def("isDetectionFrame", &IGlobalTracker::isDetectionFrame, "Getter of last process function state")

        .add_property("detectionPeriod", &IGlobalTracker::getDetectionPeriod, &IGlobalTracker::setDetectionPeriod, "face detection period")
        .add_property("bestFramesNum", &IGlobalTracker::getBestFramesNum, &IGlobalTracker::setBestFramesNum, "number of best frames to store for each track")
        .add_property("bestFaceScale", &IGlobalTracker::getBestFaceScale, &IGlobalTracker::setBestFaceScale, "specify crop region for each best face")
        .add_property("roi", &IGlobalTracker::getROI, &IGlobalTracker::setROI, "roi for detection")
        .add_property("minFaceSize", &IGlobalTracker::getMinFaceSize, &IGlobalTracker::setMinFaceSize, "minimum face size for detection")
        .add_property("maxFaceSize", &IGlobalTracker::getMaxFaceSize, &IGlobalTracker::setMaxFaceSize, "maximum face size for detection")

        .add_property("maxLostTime", &IGlobalTracker::getMaxLostTime, &IGlobalTracker::setMaxLostTime, "track lost time")
        .add_property("detectionThr", &IGlobalTracker::getDetectionThr, &IGlobalTracker::setDetectionThr, "detection threshold")
        .add_property("trackingThr", &IGlobalTracker::getTrackingThr, &IGlobalTracker::setTrackingThr, "tracking trashhold")
        .add_property("matchingThr", &IGlobalTracker::getMatchingThr, &IGlobalTracker::setMatchingThr, "matching threshold")
    ;

    /* Detection Filter */
    enum_<DETECTION_FILTER_TYPE>("DETECTION_FILTER_TYPE")
        .value("DEFAULT", DETECTION_FILTER_TYPE::DEFAULT)
    ;
    class_<IDetectionFilter, noncopyable>("IDetectionFilter", no_init)
        .def("__init__", make_constructor(&IDetectionFilter_Create, default_call_policies(), (arg("type") = DETECTION_FILTER_TYPE::DEFAULT)))
        .def("filter", &IDetectionFilter::filter, args("frame", "faces", "timestamp"), "Filter faces on key frame")
    ;

    /* Features */
    EXPORT_ARRAY(FaceFeaturePoints);

    enum_<POINTS_DETECTOR_TYPE>("POINTS_DETECTOR_TYPE")
        .value("DEFAULT", POINTS_DETECTOR_TYPE::DEFAULT)
    ;
    class_<IPointsDetector, noncopyable>("IPointsDetector", no_init)
        .def("__init__", make_constructor(&IPointsDetector_Create, default_call_policies(), (arg("type") = POINTS_DETECTOR_TYPE::DEFAULT)))
        .def("detectFromBbox", &IPointsDetector::detectFromBbox, args("image", "bbox"), "Face feature points detection from bounding box (presumably founded by face detection)")
        .def("detectFromPoints", &IPointsDetector::detectFromPoints, args("image", "points"), "Face feature points detection from initial estimation or previous position")
        .def("detect", &IPointsDetector::detectFromBbox, args("image", "bbox"), "Face feature points detection from bounding box (presumably founded by face detection)")
        .def("detect", &IPointsDetector::detectFromPoints, args("image", "points"), "Face feature points detection from initial estimation or previous position")
    ;

    /* Recognition */
    enum_<RECOGNITION_ALGO>("RECOGNITION_ALGO")
        .value("ALG1", RECOGNITION_ALGO::ALG1)
        .value("DEFAULT", RECOGNITION_ALGO::DEFAULT)
    ;

    class_<IExtractor, noncopyable>("IExtractor", no_init)
        .def("__init__", make_constructor(&IExtractor_Create, default_call_policies(), (arg("type") = RECOGNITION_ALGO::DEFAULT)))
        .def("enroll", &IExtractor::enroll, args("image", "features"), "Descriptor extraction algorithm")
    ;

    class_<IMatcher, noncopyable>("IMatcher", no_init)
        .def("__init__", make_constructor(&IMatcher_Create, default_call_policies(), (arg("type") = RECOGNITION_ALGO::DEFAULT)))
        .def("match", &IMatcher::match, args("desc1", "desc2"), "Match two descriptors")
    ;

    class_<Age>("Age", no_init)
        .def_readonly("value", &Age::value, "age")
        .def_readonly("variance", &Age::variance, "age variance")
    ;

    enum_<Gender::GENDERS>("GENDERS")
        .value("MALE", Gender::GENDERS::MALE)
        .value("FEMALE", Gender::GENDERS::FEMALE)
    ;

    class_<Gender>("Gender", no_init)
        .def_readonly("value", &Gender::value, "gender")
        .def_readonly("probs", &Gender::probs, "classification probability")
    ;

    enum_<Glasses::GLASSES>("GLASSES")
        .value("NONE", Glasses::GLASSES::NONE)
        .value("SIMPLE", Glasses::GLASSES::SIMPLE)
        .value("SUN", Glasses::GLASSES::SUN)
    ;

    class_<Glasses>("Glasses", no_init)
        .def_readonly("value", &Glasses::value, "glasses")
        .def_readonly("probs", &Glasses::probs, "classification probability")
    ;

    enum_<Race::RACES>("RACES")
        .value("WHITE", Race::RACES::WHITE)
        .value("BLACK", Race::RACES::BLACK)
        .value("ASIAN", Race::RACES::ASIAN)
        .value("OTHER", Race::RACES::OTHER)
    ;

    class_<Race>("Race", no_init)
        .def_readonly("value", &Race::value, "race")
        .def_readonly("probs", &Race::probs, "classification probability")
    ;

    class_<RecognitionQuality::tagFace>("FaceRecognitionQuality", no_init)
        .def_readonly("value", &RecognitionQuality::tagFace::value)
        .def_readonly("prob", &RecognitionQuality::tagFace::prob)
    ;

    class_<RecognitionQuality>("RecognotionQuality", no_init)
        .def_readonly("face", &RecognitionQuality::face, "face probability in detection region")
    ;

    class_<IClassifier, noncopyable>("IClassifier", no_init)
        .def("__init__", make_constructor(&IClassifier_Create, default_call_policies(), (arg("type") = RECOGNITION_ALGO::DEFAULT)))
        .def("classifyAge", &IClassifier::classifyAge, args("desc"), "Classify age")
        .def("classifyGender", &IClassifier::classifyGender, args("desc"), "Classify gender")
        .def("classifyGlasses", &IClassifier::classifyGlasses, args("desc"), "Classify glasses")
        .def("classifyRace", &IClassifier::classifyRace, args("desc"), "Classify race")
        .def("classifyQuality", &IClassifier::classifyQuality, args("desc"), "Classify quality")
    ;
}
