#include <vector>
#include <algorithm>
#include <iostream>
#include <array>
#include <sstream>
#include <ostream>
#include <memory>
#include <cmath>

/* BOOST */
#define BOOST_PYTHON_STATIC_LIB
#include <boost/python.hpp>

/* FACESDK */
#include "tvacore/image.h"
#include "videoio/IVideoReader.h"

using namespace TVA::Core;

using boost::noncopyable;

using boost::python::object;
using boost::python::def;
using boost::python::self;
using boost::python::class_;
using boost::python::arg;
using boost::python::args;
using boost::python::make_constructor;
using boost::python::default_call_policies;
using boost::python::no_init;
using boost::python::scope;
using boost::python::docstring_options;
using boost::python::make_tuple;
using boost::python::extract;

using std::shared_ptr;
using std::size_t;

using TVA::VideoIO::IVideoReader;
using TVA::VideoIO::TimeStamp;

object IVideoReader_read(IVideoReader &self)
{
    Image8U frame;
    TimeStamp ts;

    if(!self.read(frame, ts)) {
        return object();
    }

    return make_tuple(frame, ts);
}

shared_ptr<IVideoReader> IVideoReader_Create(const object &resource, const size_t buffer_size)
{
    extract<int> device_id(resource);
    if(device_id.check())
        return IVideoReader::create(device_id(), buffer_size);
    else
        return IVideoReader::create(extract<std::string>(resource), buffer_size);
}

BOOST_PYTHON_MODULE(pyvideoio)
{
    scope().attr("__doc__") = "TVA::VideoIO module";
    docstring_options docopt;
    docopt.enable_all();
    docopt.disable_cpp_signatures();

    class_<IVideoReader, noncopyable>("IVideoReader", no_init)
        .def("__init__", make_constructor(&IVideoReader_Create, default_call_policies(), args("resource", "buffer_size")))
        .def("read", &IVideoReader_read, "read frame")
        .def_readonly("width", &IVideoReader::getWidth, "stream width")
        .def_readonly("height", &IVideoReader::getHeight, "stream height")
        .def_readonly("fps", &IVideoReader::getFPS, "stream fps")
        .def(!self)
    ;
}
