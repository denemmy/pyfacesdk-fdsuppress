#!/bin/sh

#building boost
wget http://sourceforge.net/projects/boost/files/boost/1.60.0/boost_1_60_0.tar.gz/download -O boost_1_60_0.tar.gz
tar -xvf boost_1_60_0.tar.gz
rm boost_1_60_0.tar.gz
cd boost_1_60_0
./bootstrap.sh
./bjam --v2 cxxflags=-fPIC link=static address-model=$1 architecture=x86 --with-python variant=release --prefix=../3rdparty/boost/ install
cd ..
rm -rf boost_1_60_0

#building v4l2-utils
wget https://linuxtv.org/downloads/v4l-utils/v4l-utils-1.10.0.tar.bz2
tar -xvf v4l-utils-1.10.0.tar.bz2
rm v4l-utils-1.10.0.tar.bz2
cd v4l-utils-1.10.0
v4l2_prefix=$(pwd -P)/../3rdparty/v4l2-utils/
./configure --enable-static --disable-shared --prefix=$v4l2_prefix --disable-qv4l2 --disable-doxygen-doc --disable-doxygen-dot --disable-doxygen-html --disable-doxygen-pdf --with-pic --disable-rpath -disable-libdvbv5 --without-jpeg
make -j9
mkdir -p $v4l2_prefix/include
cp lib/include/*.h $v4l2_prefix/include/
mkdir -p $v4l2_prefix/lib
cp lib/libv4l1/.libs/libv4l1.a $v4l2_prefix/lib/
cp lib/libv4l2/.libs/libv4l2.a $v4l2_prefix/lib/
cp lib/libv4lconvert/.libs/libv4lconvert.a $v4l2_prefix/lib/
cd ..
rm -rf v4l-utils-1.10.0

#building ffmpeg
wget https://www.ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
tar -xvf ffmpeg-snapshot.tar.bz2
rm ffmpeg-snapshot.tar.bz2
cd ffmpeg
./configure --pkg-config=pkg-config-nope --disable-debug --enable-nonfree --enable-version3 --enable-gpl --disable-programs --disable-doc --enable-avdevice --enable-avcodec --enable-avformat --enable-swresample --enable-swscale --enable-postproc --enable-avfilter --enable-pic --enable-lto --ar=gcc-ar --ranlib=gcc-ranlib --prefix=../3rdparty/ffmpeg/ --disable-libxcb --disable-libxcb-shape --disable-libxcb-shm --disable-libxcb-xfixes --disable-x11grab --disable-xlib --disable-vaapi --disable-vdpau --disable-mipsdsp --disable-d3d11va --disable-dxva2 --disable-vda --enable-pthreads --disable-w32threads --disable-os2threads --disable-encoders --disable-muxers --disable-sdl --disable-outdevs --disable-filters --disable-yasm --disable-indev=dv1394 --disable-indev=fbdev --disable-lzma --disable-iconv --extra-cflags=-I/../3rdparty/v4l2-utils/include
--extra-ldflags=-L/../3rdparty/v4l2-utils/lib --enable-libv4l2

make -j9
make install
cd ..
rm -rf ffmpeg
